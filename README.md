![Pipeline status](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cuda/badges/main/pipeline.svg?ignore_skipped=true) ![C++ coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cuda/badges/main/coverage.svg?job=coverage:ubuntu_cpp&key_text=C%2B%2B+coverage&key_width=90) ![Python coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cuda/badges/main/coverage.svg?job=coverage:ubuntu_python&key_text=Python+coverage&key_width=100)

# Aidge CUDA library

You can find in this folder the library that implements the CUDA operators.

## Pip installation

You will need to install first the aidge_core library before installing aidge_backend_cuda.
Also, make sure that the install path was set before installing aidge_core library.
Then run in your python environnement : 
``` bash
pip install . -v
```

## Standard C++ Compilation

You will need to compile first the Core library before compiling the CUDA one.
The makefile is designed to do it for you.
