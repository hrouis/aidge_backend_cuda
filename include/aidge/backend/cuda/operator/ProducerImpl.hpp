/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CUDA_OPERATOR_PRODUCERIMPL_H_
#define AIDGE_CUDA_OPERATOR_PRODUCERIMPL_H_

#include <memory>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
class ProducerImpl_cuda : public OperatorImpl {
public:
    ProducerImpl_cuda(const Producer_Op &op) : OperatorImpl(op) {}

    static std::unique_ptr<ProducerImpl_cuda> create(const Producer_Op &op) {
        return std::make_unique<ProducerImpl_cuda>(op);
    }

    NbElts_t getNbProducedData(const IOIndex_t outputIdx) const override final;
    void forward() override;
};

namespace {
static Registrar<Producer_Op> registrarProducerImpl_cuda("cuda", Aidge::ProducerImpl_cuda::create);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CUDA_OPERATOR_PRODUCERIMPL_H_ */
