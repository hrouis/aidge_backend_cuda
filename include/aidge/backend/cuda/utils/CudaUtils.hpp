#ifndef AIDGE_BACKEND_CUDA_CUDA_UTILS_H
#define AIDGE_BACKEND_CUDA_CUDA_UTILS_H

#include <string>
#include <memory>
#include <sstream>
#include <iostream>
#include <stdexcept>

#include <cublas_v2.h>
#include <cuda.h>
#include <cudnn.h>

#define CHECK_CUDNN_STATUS(status)                                             \
    do {                                                                       \
        const cudnnStatus_t e = (status);                                      \
        if (e != CUDNN_STATUS_SUCCESS) {                                       \
            std::stringstream error;                                           \
            error << "CUDNN failure: " << cudnnGetErrorString(e) << " ("       \
                  << static_cast<int>(e) << ") in " << __FILE__ << ':' << __LINE__;         \
            int status_dev;                                                           \
            if (cudaGetDevice(&status_dev) == cudaSuccess)                            \
                error << " on device #" << status_dev;                                \
            std::cerr << error.str() << std::endl;                             \
            cudaDeviceReset();                                                 \
            throw std::runtime_error(error.str());                             \
        }                                                                      \
    } while(0)

#define CHECK_CUDA_STATUS(status)                                              \
    do {                                                                       \
        const cudaError_t e = (status);                                        \
        if ((e) != cudaSuccess) {                                              \
            std::stringstream error;                                           \
            error << "Cuda failure: " << cudaGetErrorString(e) << " ("         \
                  << static_cast<int>(e) << ") in " << __FILE__ << ':' << __LINE__;         \
            int status_dev;                                                           \
            if (cudaGetDevice(&status_dev) == cudaSuccess)                            \
                error << " on device #" << status_dev;                                \
            std::cerr << error.str() << std::endl;                             \
            cudaDeviceReset();                                                 \
            throw std::runtime_error(error.str());                             \
        }                                                                      \
    } while(0)

#define CHECK_CUBLAS_STATUS(status)                                            \
    do {                                                                       \
        const cublasStatus_t e = (status);                                     \
        if (e != CUBLAS_STATUS_SUCCESS) {                                      \
            std::stringstream error;                                           \
            error << "Cublas failure: "                                        \
                  << Aidge::Cuda::cublasGetErrorString(e) << " ("               \
                  << static_cast<int>(e) << ") in " << __FILE__ << ':' << __LINE__;         \
            int status_dev;                                                           \
            if (cudaGetDevice(&status_dev) == cudaSuccess)                            \
                error << " on device #" << status_dev;                                \
            std::cerr << error.str() << std::endl;                             \
            cudaDeviceReset();                                                 \
            throw std::runtime_error(error.str());                             \
        }                                                                      \
    } while(0)

namespace Aidge {
namespace Cuda {
    const char* cublasGetErrorString(cublasStatus_t error);

    // Enable Peer-to-Peer communications between devices
    // when it is possible
    void setMultiDevicePeerAccess(unsigned int size, unsigned int* devices);
}
}

#endif // AIDGE_BACKEND_CUDA_CUDA_UTILS_H
