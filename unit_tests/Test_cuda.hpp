#include <cuda.h>
#include <cuda_runtime.h>

#include "aidge/backend/cuda/utils/CudaUtils.hpp"

void vector_add(float *out, float *a, float *b, int n);
