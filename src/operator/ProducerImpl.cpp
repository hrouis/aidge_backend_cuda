/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cassert>
#include <numeric> // std::accumulate
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/operator/ProducerImpl.hpp"

Aidge::DimSize_t Aidge::ProducerImpl_cuda::getNbProducedData(
    Aidge::IOIndex_t outputIdx) const
{
    // Requires the whole tensors, regardless of available data on inputs
    assert(outputIdx == 0 && "operator has only one output");
    (void) outputIdx;

    return std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->size();
}

void Aidge::ProducerImpl_cuda::forward()
{
}
